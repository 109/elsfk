/**
 * Created by Administrator on 2018/5/20.
 */
var local = function(){
    //游戏对象
    var game;

    //绑定键盘事件
    var bindKeyEvent = function(){
       document.onkeydown = function(e){
            if(e.keyCode == 38){ //up
                game.rotate();
                console.log('旋转');
               /* game.up();
                console.log('上');*/
            }else if(e.keyCode == 39){ //right
                game.right();
                console.log('右');
            }else if(e.keyCode == 40){ //down
                game.down();
                console.log('下')
            }else if(e.keyCode== 37){ //left
                game.left();
                console.log('左')
            }else if(e.keyCode == 32){ //space
                game.fall();
                console.log('空格下')
            }else if(e.keyCode == 13){
                game.rotate();
                console.log('旋转');
            }
       }
    };


    //    开始
    var start = function(){
        var doms = {
            gameDiv: document.getElementById('game'),
            nextDiv: document.getElementById('next')
        };
        game = new Game();
        game.init(doms);
        bindKeyEvent();
    };
    //导出API
    this.start = start;
};